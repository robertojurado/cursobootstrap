$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto1').on('show.bs.modal', function (e){
        console.log('el modal contacto se está mostrando');
        $('#contactoBtn').removeClass('btn btn-info');
        $('#contactoBtn').addClass('btn btn-secondary');
        $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto1').on('shown.bs.modal', function (e){
        console.log('el modal contacto se mostró');
    });

    $('#contacto1').on('hide.bs.modal', function (e){
        console.log('el modal contacto se oculta');
    });

    $('#contacto1').on('hidden.bs.modal', function (e){
        console.log('el modal contacto se ocultó');
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').removeClass('btn btn-secondary');
        $('#contactoBtn').addClass('btn btn-info');
    });

    $('#contacto2').on('show.bs.modal', function (e){
        console.log('el modal contacto se está mostrando');
        $('#comentarios').removeClass('btn btn-info');
        $('#comentarios').addClass('btn btn-secondary');
        $('#comentarios').prop('disabled', true);
    });

    $('#contacto2').on('shown.bs.modal', function (e){
        console.log('el modal contacto se mostró');
    });

    $('#contacto2').on('hide.bs.modal', function (e){
        console.log('el modal contacto se oculta');
    });

    $('#contacto2').on('hidden.bs.modal', function (e){
        console.log('el modal contacto se ocultó');
        $('#comentarios').prop('disabled', false);
        $('#comentarios').removeClass('btn btn-secondary');
        $('#comentarios').addClass('btn btn-info');
    });

});


    

